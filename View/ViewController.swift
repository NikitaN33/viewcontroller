//
//  ViewController.swift
//  View
//
//  Created by Nikita Nechyporenko on 19.06.18.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

var str = ""

class ViewController: UIViewController {
    
    @IBAction func buttonPressed() {
        headerLabel.text = "Hello world!"
        headerLabel.textColor = UIColor.red
    }
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var myTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print(#function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print(#function)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(#function)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(#function)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(#function)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(#function)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print(#function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(#function)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
//        print(#function)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        str = myTextField.text ?? ""
    }
   
    


}

